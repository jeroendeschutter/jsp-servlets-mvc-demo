<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Message Board 0.1 - New message</title>
</head>
<body>
<div style="font-family: 'Open Sans', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif';">
<h1>New message</h1>
<form action="${pageContext.request.contextPath}/MvcController/saveMessage">
<p>Subject : <input type="text" name="subject" /></p>
<p>Body : <br /><input type="text" name="body" /></p>
<p><input type="submit" value="Add message"/></p>
</form>
</div>
</body>
</html>