<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Message Board 0.1 - Overview</title>
</head>
<body>
<div style="font-family: 'Open Sans', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif';">
    <p>
        <c:choose>
            <c:when test="${empty user}">
                <a href="${pageContext.request.contextPath}/MvcController/login">Log in</a>
            </c:when>
            <c:otherwise>
                Welcome ${user} !
            </c:otherwise>
        </c:choose>
    </p>
    <div style="width: 50%;margin-left: auto;margin-right: auto;">
        <h1>Message Board 0.1</h1>
        <c:if test="${!empty user}">
            <p><a href="${pageContext.request.contextPath}/MvcController/newMessage">New message</a></p>
        </c:if>
        <c:forEach items="${allMessages}" var="message">
            <div style="width: 50%;background-color: aliceblue;margin-left: auto;margin-right: auto;padding: 16px;margin-top: 16px">
                <small>${message.value.user}</small>
                <h3>${message.value.subject}</h3>
                <p>${message.value.body}</p>
                <small>Posted ${message.value.created}</small>
                <c:if test="${! empty message.value.updated}">
                    <br /><small>Last edited ${message.value.updated}</small>
                </c:if>

                <div style="text-align: right">
                    <small>
                        <c:if test="${message.value.user == user}">
                            <a href="${pageContext.request.contextPath}/MvcController/editMessage?msgId=${message.value.id}">Edit</a>
                            &nbsp;-&nbsp;
                            <a href="${pageContext.request.contextPath}/MvcController/deleteMessage?msgId=${message.value.id}">Delete</a>
                        </c:if>
                    </small>
                </div>
            </div>
        </c:forEach>

    </div>

</div>

</body>
</html>