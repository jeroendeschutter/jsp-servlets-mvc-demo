package com.realdolmen.jdcq390.servlets;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/MvcController/*")
public class MvcController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		// Initialization of dummy in-memory data repository + add 1 example message
		HashMap<UUID, Message> allMessages = new HashMap<>();
		Message welcome = new Message();
		welcome.setUser("Message Board development team.");
		welcome.setSubject("Welcome to MessageBoard 0.1");
		welcome.setBody("This is a demo application for a MVC architecture with JSP and Servlets.\n" +
				"Please log in to post messages.");
		allMessages.put(welcome.getId(), welcome);
		config.getServletContext().setAttribute("allMessages", allMessages);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		RequestDispatcher dispatcher;
		Map<UUID, Message> allMessages;
		Message message;
		String uuid;
		UUID messageId;
		
		String action = request.getPathInfo();
		switch (action) {
		case "/login":
			if (request.getSession().getAttribute("user") == null) {
				String user = request.getParameter("userName");
				if (user == null) {
					response.sendRedirect("../login.jsp");
					break;
				} else {
					request.getSession().setAttribute("user", user);
				}
			}
			response.sendRedirect("./overview");
			
			break;
			
		case "/newMessage" :

			dispatcher = request.getRequestDispatcher("/WEB-INF/newMessage.jsp");
			dispatcher.forward(request, response);
			break;
			
		case "/saveMessage" :
			String user = (String) request.getSession().getAttribute("user");
			allMessages = (Map<UUID, Message>) request.getServletContext().getAttribute("allMessages");
			uuid = (String) request.getParameter("msgId");
			if (uuid != null) { // update existing message
				messageId = UUID.fromString(uuid);
				message = allMessages.get(messageId);
				message.setUpdated(new Date());
			} else {
				message = new Message();
				message.setUser(user);
			}
			message.setSubject(request.getParameter("subject"));
			message.setBody(request.getParameter("body"));
			
			allMessages.put(message.getId(), message);
			request.getServletContext().setAttribute("allMessages", allMessages);
			response.sendRedirect("./overview");	
			break;
			
		case "/deleteMessage" :
			uuid = (String) request.getParameter("msgId");
			messageId = UUID.fromString(uuid);
			allMessages = (Map<UUID, Message>) request.getServletContext().getAttribute("allMessages");
			allMessages.remove(messageId);
			request.getServletContext().setAttribute("allMessages", allMessages);
			response.sendRedirect("./overview");
			break;
			
		case "/editMessage" :
			uuid = (String) request.getParameter("msgId");
			messageId = UUID.fromString(uuid);
			allMessages = (Map<UUID, Message>) request.getServletContext().getAttribute("allMessages");
			message = allMessages.get(messageId);
			request.setAttribute("message", message);
		
			dispatcher = request.getRequestDispatcher("/WEB-INF/editMessage.jsp");
			dispatcher.forward(request, response);
			break;			
			
		case "/overview" :
			dispatcher = request.getRequestDispatcher("/WEB-INF/overview.jsp");
			dispatcher.forward(request, response);
			
			break;
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
