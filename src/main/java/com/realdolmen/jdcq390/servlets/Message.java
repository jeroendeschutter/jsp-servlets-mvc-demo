package com.realdolmen.jdcq390.servlets;

import java.util.Date;
import java.util.UUID;

public class Message {
	
	private String subject;
	private String body;
	private String user;
	private Date created;
	private Date updated;
	private UUID id;
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public Date getCreated() {
		return created;
	}

	public Date getUpdated() {
		return updated;
	}
	public void setUpdated(Date updated) {
		this.updated = updated;
	}
	public UUID getId() {
		return id;
	}
	
	public Message() {
		this.id = UUID.randomUUID();
		this.created = new Date();
	}
	
	@Override
	public String toString() {
		return "Message [subject=" + subject + ", body=" + body + ", user=" + user + ", created=" + created
				+ ", id=" + id + "]";
	}

}
