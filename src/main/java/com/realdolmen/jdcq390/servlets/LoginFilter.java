package com.realdolmen.jdcq390.servlets;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebFilter({"/MvcController/addMessage", 
	"/MvcController/editMessage", 
	"/MvcController/deleteMessage", 
	"/MvcController/newMessage"})
public class LoginFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();
		if (session.getAttribute("user") == null) {
			((HttpServletResponse) response).sendRedirect("./login");
		} else {
			chain.doFilter(request, response);
		}
		
	}

	@Override
	public void destroy() {}

	@Override
	public void init(FilterConfig config) throws ServletException {}

}
