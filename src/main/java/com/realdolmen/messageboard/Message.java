package com.realdolmen.messageboard;

import java.util.Date;
import java.util.UUID;

public class Message {

    UUID id;
    User user;
    String title;
    String body;
    Date created;
    Date modified;

    public Message(User user, String title, String body) {
        this.id = UUID.randomUUID();
        this.user = user;
        this.title = title;
        this.body = body;
        this.created = new Date();
    }
}
